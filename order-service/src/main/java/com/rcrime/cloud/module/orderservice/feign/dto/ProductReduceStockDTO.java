package com.rcrime.cloud.module.orderservice.feign.dto;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * 商品减少库存 DTO
 */
@Data
@Accessors(chain = true)
public class ProductReduceStockDTO {

    /**
     * 商品编号
     */
    private Long productId;
    /**
     * 数量
     */
    private Integer amount;


}
