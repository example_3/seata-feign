package com.rcrime.cloud.module.orderservice.service.impl;

import com.rcrime.cloud.module.orderservice.dao.OrderDao;
import com.rcrime.cloud.module.orderservice.entity.OrderDO;
import com.rcrime.cloud.module.orderservice.feign.AccountServiceFeignClient;
import com.rcrime.cloud.module.orderservice.feign.ProductServiceFeignClient;
import com.rcrime.cloud.module.orderservice.feign.dto.AccountReduceBalanceDTO;
import com.rcrime.cloud.module.orderservice.feign.dto.ProductReduceStockDTO;
import com.rcrime.cloud.module.orderservice.feign.*;
import com.rcrime.cloud.module.orderservice.feign.dto.*;
import com.rcrime.cloud.module.orderservice.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {


    @Resource
    private OrderDao orderDao;

    @Resource
    private AccountServiceFeignClient accountService;
    @Resource
    private ProductServiceFeignClient productService;

    @Override
    @GlobalTransactional
    public Integer createOrder(Long userId, Long productId, Integer price) {
        Integer amount = 1; // 购买数量，暂时设置为 1。

        log.info("[createOrder] 当前 XID: {}", RootContext.getXID());

        // 扣减库存
        productService.reduceStock(new ProductReduceStockDTO().setProductId(productId).setAmount(amount));

        // 扣减余额
        accountService.reduceBalance(new AccountReduceBalanceDTO().setUserId(userId).setPrice(price));

        // 保存订单
        OrderDO order = new OrderDO().setUserId(userId).setProductId(productId).setPayAmount(amount * price);
        orderDao.saveOrder(order);
        log.info("[createOrder] 保存订单: {}", order.getId());

        // 返回订单编号
        return order.getId();
    }

}
